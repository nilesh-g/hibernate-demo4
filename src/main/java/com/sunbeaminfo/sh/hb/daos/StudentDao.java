package com.sunbeaminfo.sh.hb.daos;

import org.hibernate.Session;

import com.sunbeaminfo.sh.hb.entities.StudStdRoll;
import com.sunbeaminfo.sh.hb.entities.Student;
import com.sunbeaminfo.sh.hb.utils.HbUtil;

public class StudentDao {
	public Student getStudent(int std, int roll) {
		Session session = HbUtil.getSession();
		StudStdRoll id = new StudStdRoll(std, roll);
		return session.get(Student.class, id);
	}
}
