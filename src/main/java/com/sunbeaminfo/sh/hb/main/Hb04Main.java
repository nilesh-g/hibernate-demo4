package com.sunbeaminfo.sh.hb.main;

import java.util.List;

import com.sunbeaminfo.sh.hb.daos.BookDao;
import com.sunbeaminfo.sh.hb.daos.StudentDao;
import com.sunbeaminfo.sh.hb.entities.Book;
import com.sunbeaminfo.sh.hb.entities.Student;
import com.sunbeaminfo.sh.hb.utils.HbUtil;

public class Hb04Main {
	public static void main(String[] args) {
		/*
		BookDao dao = new BookDao();
		try {
			HbUtil.beginTransaction();
			Book b = new Book(0, "Atlas Shrugged", "Rand", "Novell", 723.23);
			dao.addBook(b);
			HbUtil.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		
		try {
			HbUtil.beginTransaction();
			List<Book> list = dao.getBooks();
			for (Book b : list)
				System.out.println(b);
			HbUtil.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		*/
		
		StudentDao dao = new StudentDao();
		try {
			HbUtil.beginTransaction();
			Student st = dao.getStudent(2, 1);
			System.out.println("Found : " + st);
			HbUtil.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		HbUtil.shutdown();
	}
}
